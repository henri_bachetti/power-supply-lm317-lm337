# LM317 & LM337 POWER-SUPPLY

The purpose of this page is to explain step by step the realization of an LM317/LM337 dual power supply.

It has been designed to power a preamplifier or an active crossover.

The power supply uses the following components :

 * 1 2x12V, 2x15V or 2x2x18V transformer
 * 1 rectifier bridge
 * 2 4700µF 35V capacitors
 * 2 100nF and 2 100pF capacitors
 * 1 LM317 & 1 LM337 chips

### ELECTRONICS

The schema is made using KICAD.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2021/09/audio-alimentation-symetrique-faible.html

